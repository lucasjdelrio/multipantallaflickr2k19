// Import a library to help create a component
import React from 'react';
import { AppRegistry, Text, View, Image, Linking } from 'react-native';
import AlbumList from './src/components/AlbumList';
import PhotoList from './src/components/PhotoList';
import { Router, Scene } from 'react-native-router-flux';

// Create a component
const App = () => (
 
      <Router>

        <Scene key="root">
          <Scene key="albumList" component={AlbumList} title="Directorios" initial={true} />
          <Scene key="photoList" component={PhotoList} title="Imagenes" />
        </Scene>

      </Router>
);

// Render it to the device
AppRegistry.registerComponent('albums', () => App);


export default App;