import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';
import { Actions } from 'react-native-router-flux';

/*
{ "photosets": { "page": 1, "pages": 1, "perpage": 3, "total": 3, 
    "photoset": [
      { "id": "72157675030263626", "primary": "22356251255", "secret": "029f8e1281", "server": "5718", "farm": 6, "photos": 2, "videos": 0, 
        "title": { "_content": "prueba" }, 
        "description": { "_content": "" }, "needs_interstitial": 0, "visibility_can_see_set": 1, "count_views": 8, "count_comments": 0, "can_comment": 0, "date_create": "1476306576", "date_update": "1476306577" },
      { "id": "72157660115185712", "primary": "22343185752", "secret": "ed4f2a6f51", "server": "582", "farm": 1, "photos": 3, "videos": 0, 
        "title": { "_content": "Album 1" }, 
        "description": { "_content": "" }, "needs_interstitial": 0, "visibility_can_see_set": 1, "count_views": 4, "count_comments": 0, "can_comment": 0, "date_create": "1445426958", "date_update": "1445427559" },
      { "id": "72157660115126402", "primary": "22343185752", "secret": "ed4f2a6f51", "server": "582", "farm": 1, "photos": 3, "videos": 0, 
        "title": { "_content": "Album 2" }, 
        "description": { "_content": "Estes es un album" }, "needs_interstitial": 0, "visibility_can_see_set": 1, "count_views": 5, "count_comments": 0, "can_comment": 0, "date_create": "1445426832", "date_update": "1445428332" }
    ] }, "stat": "ok" }
*/
    
const AlbumDetail = ({ title, albumId, photos, videos, description, comments, views, api_key, user_id }) => {
  const {
    headerContentStyle,
    infoStyle,
    headerTextStyle,
    imageStyle
  } = styles;

  if (description == "") {
    description = "Sin descripción."
  }

  return (
    <Card>

      <CardSection>
        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>{title}</Text>
        </View>
      </CardSection>

      <CardSection>
        <View style={headerContentStyle}>
          <Text style={infoStyle}>{description}</Text>
          <Text style={infoStyle}>{photos} imágenes</Text>
          <Text style={infoStyle}>{videos} videos</Text>
          <Text style={infoStyle}>{views} visitas</Text>
          <Text style={infoStyle}>{comments} comentarios</Text>
        </View>
      </CardSection>

      <CardSection>
        <Button onPress={() => Actions.photoList({albumId:albumId, api_key:api_key, user_id:user_id})}>
          Ver album!
        </Button>
      </CardSection>

    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  }
};

export default AlbumDetail;