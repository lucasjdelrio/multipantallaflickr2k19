import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';

class AlbumList extends Component {
  state = { photoset: null };

  /*
Si se desea uso genérico, conociendo "api_key" y "user_id" puedo ejecutar el siguiente bloque

  componentWillMount() {
    axios.get('https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=${this.props.api_key}&user_id=${this.props.user_id}&format=json&nojsoncallback=1')
      .then(response => this.setState({ photoset: response.data.photosets.photoset }));
  }
  */

  componentWillMount() {
    axios.get('https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&user_id=137290658%40N08&format=json&nojsoncallback=1')
      .then(response => this.setState({ photoset: response.data.photosets.photoset }));
  }

//"user_id" se traduce de '137290658%40N08' a '137290658@N08' debido a % en URL (v8)

//Se llama a componente <AlbumDetail> y se asignan parametros 
  renderAlbums() {
    return this.state.photoset.map(album =>
      <AlbumDetail title={album.title._content} albumId={album.id}
      photos={album.photos} videos={album.videos} description={album.description._content} 
      comments={album.count_comments} views={album.count_views}
      api_key={'6e8a597cb502b7b95dbd46a46e25db8d'} user_id={'137290658@N08'}/>
    );
  }

/*
Si se desea uso genérico, conociendo "api_key" y "user_id" puedo ejecutar el siguiente bloque

  renderAlbums() {
    return this.state.photoset.map(album =>
      <AlbumDetail title={album.title._content} albumId={album.id}
      photos={album.photos} videos={album.videos} description={album.description._content} 
      comments={album.count_comments} views={album.count_views}
      api_key={this.props.api_key} user_id={this.props.user_id}/>
    );
  }
*/

  render() {
    console.log(this.state);


    if (!this.state.photoset) { 
			return (
					<Text>
            Loading. Wait for us...
					</Text>
				);
    }

    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          {this.renderAlbums()}
        </ScrollView>
      </View>
    );
  }
}

export default AlbumList;