import React, { Component } from "react";
import { Text, ActivityIndicator, View, FlatList, Image } from "react-native";
import Axios from "axios";

import PhotoDetail from "./PhotoDetail";

export default class PhotoList extends Component {
  constructor(props) {
    super(props);
    this.state = { cargandoFotos: true, errorCargandoFotos: "", fotos: null };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    await this.buscarFotos();
    console.log(this.state);
  };

  apiFlicker = () => {
    return Axios.get(
      `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id=${
        this.props.albumId
      }&user_id=137290658%40N08&format=json&nojsoncallback=1`
    );
  };

  buscarFotos = async () => {
    try {
      this.setState({ cargandoFotos: true });
      var respuesta = await this.apiFlicker();
      respuesta = respuesta.data.photoset.photo;
      console.log(respuesta);

      if (respuesta == null || respuesta == "") {
        this.setState({
          cargandoFotos: false,
          errorCargaFotos: "...error al cargar las fotos"
        });
        return;
      }
      this.setState({ fotos: respuesta, cargandoFotos: false });
    } catch (ex) {
      this.setState({
        cargandoFotos: false,
        errorCargaFotos: "...error al cargar las fotos"
      });
    }
  };

  renderSeparador = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: "#cfd8dc"
        }}
      />
    );
  };

  renderFoto = item => {
    return (
      <View
        style={{
          borderBottomWidth: 1,
          padding: 5,
          backgroundColor: "red",
          justifyContent: "flex-start",
          flexDirection: "row",
          borderColor: "#ddd",
          position: "relative"
        }}
      >
        <Image
          style={{ height: 300, width: null, flex: 1 }}
          // style={{ height: 1000, width: null }}
          source={{
            uri: `https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}.jpg`
          }}
        />
      </View>
    );
  };

  render() {
    if (this.state.cargandoFotos) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ActivityIndicator size="large" color="#78909c" />
        </View>
      );
    }

    return (
      <View>
        <FlatList
          data={this.state.fotos}
          renderItem={({ item }) => (
            <PhotoDetail
              title={item.title}
              imageUrl={`https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}.jpg`}
            />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}
//"user_id" se traduce de '137290658%40N08' a '137290658@N08' debido a % en URL (v8)

/*
Consumo de API flickr.photosets.getPhotos
Parámetros obligatorios
api_key: 6e8a597cb502b7b95dbd46a46e25db8d 
user_id: 137290658@N08
photoset_id: albumId, por ej: 72157660115185712		
*/


  /*
      axios.get(`https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id=${this.props.albumId}&user_id=137290658%40N08&format=json&nojsoncallback=1`)

  axios.get(`https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${this.props.api_key}&photoset_id=${this.props.albumId}&user_id=${this.props.user_id}&format=json&nojsoncallback=1&per_page=${per_page}&page=${page}`)
  */

/*
https://www.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id=72157660115185712&user_id=137290658%40N08&format=json&nojsoncallback=1

Response
{ "photoset":{ "id": "72157660115185712", "primary": "22343185752", "owner": "137290658@N08", "ownername": "pablo.szyrko", 
    "photo": [
      { "id": "22343185752", "secret": "ed4f2a6f51", "server": "582", "farm": 1, "title": "Tigre", "isprimary": 1, "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "22356251255", "secret": "029f8e1281", "server": "5718", "farm": 6, "title": "images", "isprimary": 0, "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "22343185562", "secret": "8f0c9a4418", "server": "5759", "farm": 6, "title": "images (1)", "isprimary": 0, "ispublic": 1, "isfriend": 0, "isfamily": 0 }
    ], "page": 1, "per_page": "500", "perpage": "500", "pages": 1, "title": "Album 1", "total": 3 }, "stat": "ok" }
*/

  
 /* renderAlbums() {
    return this.state.photos.map(photo =>
      <PhotoDetail 
      title={photo.title} 
      imageUrl={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`}
      api_key = '6e8a597cb502b7b95dbd46a46e25db8d'
      />
    );
  }*/
